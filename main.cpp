#include <vector>
#include <queue>
#include <iostream>
#include <fstream>

using namespace std;

int main() {
    const int INF = 1000000001; // вес, больший веса максимально возможного пути
    //const int MAX_EDGES = 100000;

    int src = 1; // начальная вершина
    int dest = 4; // конечная вершина

    ifstream fin("minpath.in");
    ofstream fout("minpath.out");

    int N, M;
    fin >> N >> M >> src >> dest;

    vector<vector<pair<int, int>>> adj(N + 1);

    int u, v, wt;
    for (int i = 0; i < M; i++) {
        fin >> u >> v >> wt;
        adj[u].emplace_back(v, wt);
    }

    priority_queue<pair<int, int>, vector<pair<int, int>>, greater<>> q;
    vector<int> distance(N + 1);
    vector<int> path(N + 1);
    vector<bool> processed(N + 1);

    for (int i = 1; i <= N; i++) {
        distance[i] = INF;
    }
    distance[src] = 0;
    q.push({0, src});
    while (!q.empty()) {
        int a = q.top().second;
        q.pop();
        if (processed[a]) continue;
        processed[a] = true;
        for (auto u : adj[a]) {
            int b = u.first, w = u.second;
            if (distance[a] + w < distance[b]) {
                path[b] = a;
                distance[b] = distance[a] + w;
                q.emplace(distance[b], b);
            }
        }
    }
    if (path[dest] == 0) {
        fout << 0;
        return 0;
    }
    fout << distance[dest] << "\n" << dest << " ";
    int x = dest;
    while (path[x] != src) {
        fout << path[x] << " ";
        x = path[x];
    }
    fout << src;
}
